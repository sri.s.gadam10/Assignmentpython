Starting Point For Your Term Paper Will Be The Course Book, The Contents Of Which Will Serve As The Basis For An In-Depth Examination Of One Of The Following Questions. You Are Expected To Research And Cite From Sources Corresponding To Your Chosen Topic.

1.1 Description Of The Task

You Get (A) 4 Training Datasets And

(B) One Test Dataset, As Well As

(C) Datasets For 50 Ideal Functions. All Data Respectively Consists Of X-Y-Pairs Of Values. Structure Of All CSV-Files Provided: X Y X1 Y1 ... ... Xn Yn Your Task Is To Write A Python-Program That Uses Training Data To Choose The Four Ideal Functions Which Are The Best Fit Out Of The Fifty Provided (C) *.

I) Afterwards, The Program Must Use The Test Data Provided (B) To Determine For Each And Every X-Ypair Of Values Whether Or Not They Can Be Assigned To The Four Chosen Ideal Functions**; If So, The Program Also Needs To Execute The Mapping And Save It Together With The Deviation At Hand

Ii) All Data Must Be Visualized Logically

Iii) Where Possible, Create/ Compile Suitable Unit-Test * The Criterion For Choosing The Ideal Functions For The Training Function Is How They Minimize The Sum Of All Ydeviations Squared (Least-Square) **

The Criterion For Mapping The Individual Test Case To The Four Ideal Functions Is That The Existing Maximum Deviation Of The Calculated Regression Does Not Exceed The Largest Deviation Between Training Dataset (A) And The Ideal Function

(C) Chosen For It By More Than Factor Sqrt(2) In Order To Give Proof Of Your Skills In Python Related To This Course, You Need To Adhere To Certain Criteria When Solving The Exercise; These Criteria Are Subsequently Described Under ‘Details.

You Are Given Four Training Datasets In The Form Of Csv-Files. Your Python Program Needs To Be Able To Independently Compile A SQLite Database (File) Ideally Via Sqlalchemy And Load The Training Data Into A Single Fivecolumn Spreadsheet / Table In The File. Its First Column Depicts The X-Values Of All Functions. Table 1, At The End Of This Subsection, Shows You Which Structure Your Table Is Expected To Have. The Fifty Ideal Functions, Which Are Also Provided Via A CSV-File, Must Be Loaded Into Another Table. Likewise, The First Column Depicts The X-Values, Meaning There Will Be 51 Columns Overall. Table 2, At End Of This Subsection, Schematically Describes What Structure Is Expected.

After The Training Data And The Ideal Functions Have Been Loaded Into The Database, The Test Data (B) Must Be Loaded Line-By-Line From Another CSV-File And – If It Complies With The Compiling Criterion – Matched To One Of The Four Functions Chosen Under I (Subsection Above). Afterwards, The Results Need To Be Saved Into Another Fourcolumn-Table In The SQLite Database. In Accordance With Table 3 At End Of This Subsection, This Table Contains Four Columns With X- And Y-Values As Well As The Corresponding Chosen Ideal Function And The Related Deviation.

Finally, The Training Data, The Test Data, The Chosen Ideal Functions As Well As The Corresponding / Assigned Datasets Are Visualized Under An Appropriately Chosen Representation Of The Deviation